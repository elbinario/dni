##License

Copyleft 2014-2015 Tau & Fanta

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses/.

##Features

* Fixed non-8 digits bug
* Useful for shell scripts
* Calculate checksum letter
* Make random DNI

##Languages

This is very simple. Just edit dni.c:7 and set the language

##Bugs

* [FIXED] DNI didn't print zeros at the left of the number
